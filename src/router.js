import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home.vue'

import Login from './components/auth/Login'
import Logout from './components/auth/Logout'
import Register from './components/auth/Register'
import Images from './components/image/Images'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
        path: '/',
        name: 'home',
        component: Home
      },
      {
      path: '/login',
      name: 'login',
      component: Login,
      props: true,
      meta: {
        requiresVisitor: true,
      }
    },,
      {
      path: '/images',
      name: 'images',
      component: Images,
      props: true,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        requiresVisitor: true,
      }
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    }
  ]
})
